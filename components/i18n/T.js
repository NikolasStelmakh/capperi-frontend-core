import React, { Component } from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';
import es6template from 'es6-template/dist/es6-template.standalone';


@connect(
  state => ({
    language: state.auth.language
  })
)

export default class T extends Component {
  createMarkup(markup) {
    return {__html: markup};
  }
  render() {
    let {language = {}, children, text = '', className,variables={}} = this.props;
    let template;

    if (!children) {
      children = text;
    }
    if(variables && variables.html){
      template =  es6template.compile(language[children.toLowerCase()] || text);
      return <span className={className} dangerouslySetInnerHTML={this.createMarkup(template(variables))}></span>
    }

    if (text) {
      template = _.template(language[children.toLowerCase()] || text);
      return <span className={className}>{template(this.props)}</span>
    }

    return <span className={className}>{language[children.toLowerCase()] || children}</span>
  }
}
