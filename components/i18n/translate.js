'use strict';
import _ from 'lodash';

export default function translate(key = '') {
  return (dispatch, getState) => {
    const state = getState();
    const keys = _.get(state, 'auth.language', []);

    return keys[key.toLowerCase()] || key;
  }
}
