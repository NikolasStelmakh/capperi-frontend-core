import React, { Component, PropTypes } from 'react';
import T from './i18n/T';

export default class NoMatchPage extends Component {
  render() {
    return (
    <main className="content container">
      <h1 className="text-center">404 - <T>Page not found</T></h1>
    </main>
    );
  }
}
