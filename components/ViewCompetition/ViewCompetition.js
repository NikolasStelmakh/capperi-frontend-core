'use strict';

import React, { Component, PropTypes } from 'react';
import { ButtonToolbar, Button, Row, Col, Table, Glyphicon, ButtonGroup, Panel, PageHeader, Alert, ListGroup, ListGroupItem, Label } from 'react-bootstrap';
import { Link } from 'react-router';
import moment from 'moment';

import CompetitionBanner from './CompetitionBanner';
import RunningTimeView from './RunningTimeView';
import T from '../i18n/T';
import es6template from 'es6-template/dist/es6-template.standalone';

export default
class ViewCompetition extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { title, imageUrl, start, ownUrl, end, timezone, prizes = [], rules, description, teamName  } = this.props;

    return (
      <div>
        <PageHeader>{title}</PageHeader>

        <Row>
          <Col xs={12} sm={7} md={7}>
            {imageUrl && <CompetitionBanner
              title={title}
              imgUrl={imageUrl}
            />}
          </Col>

          <Col xs={12} sm={5} md={5}>
            <RunningTimeView start={start} end={end} timezone={timezone} />
            <div className="a2a_kit a2a_kit_size_32 a2a_default_style">
              <a className="a2a_button_facebook"></a>
              <a className="a2a_button_twitter"></a>
              <a className="a2a_button_google_plus"></a>
              <a className="a2a_button_pinterest"></a>
            </div>
            <script type="text/javascript" src="//static.addtoany.com/menu/page.js"></script>
          </Col>
        </Row>

        <h2 className="h2"><T>Prizes</T></h2>

        <ListGroup className="prizes-list">
          {prizes.map((prize, index) => {
            return (
              <ListGroupItem className="prize" key={index}>
                  <span className="prize-count">
                    <Label bsStyle="primary">{prize.amount}</Label>
                  </span>
                <span className="prize-title">&nbsp;{prize.title}</span>
              </ListGroupItem>
            );
          })}
        </ListGroup>

        {description && <div>
          <h2 className="h2"><T>Description</T></h2>

          <div className="panel panel-default">
            <div className="panel-body" dangerouslySetInnerHTML={{__html: description}}/>
          </div>
        </div>}

        <h2 className="h2"><T>Rules</T></h2>

        <div className="panel panel-default">
          <div className="panel-body" dangerouslySetInnerHTML={{__html: es6template.render(rules, {
            title,
            start: moment(start).format('L'),
            end: moment(end).format('L'),
            url: `${CLIENT_URL}/${ownUrl}`,
            totalPrizes: prizes.length,
            teamName
          })}}/>
        </div>

      </div>
    );
  }
}