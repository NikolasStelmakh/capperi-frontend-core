import React, { Component, PropTypes} from 'react';
import { Row, Panel, Col, Button } from 'react-bootstrap';
import moment from 'moment';
import T from '../i18n/T';

export default class RunningTimeView extends Component {
  render() {
    const componentTitle = <T>Running time</T>;
    let {start, end, timezone} = this.props;

    let formatStart = moment(moment(new Date(start)).utcOffset(timezone).format('YYYY-MM-DDTHH:mm:ss')).format('lll');
    let formatEnd = moment(moment(new Date(end)).utcOffset(timezone).format('YYYY-MM-DDTHH:mm:ss')).format('lll');

    return (
      <Panel className="running-time-panel" header={componentTitle}>
        <p>
          <span><T>Start</T>:&nbsp;</span>
          <span>
            {formatStart}
            <br/>
            ({timezone})
          </span>
        </p>

        <p>
          <span><T>End</T>:&nbsp;</span>
          <span>{formatEnd}&nbsp;</span>
          <br/>
          ({timezone})
        </p>

        <p>
          <span><T>Total time</T>:&nbsp;</span>
          <span>{moment(new Date(end)).from(new Date(start), true)}</span>
        </p>
      </Panel>
    )
  }
}