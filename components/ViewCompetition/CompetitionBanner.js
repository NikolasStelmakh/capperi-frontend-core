import React, { Component, PropTypes} from 'react';

export default class CompetitionBanner extends Component {
  static propTypes = {
    title: PropTypes.string,
    imgUrl: PropTypes.string
  };

  render() {
    const { title, imgUrl  } = this.props;

    return (
      <div className="competition-banner">
        <img
          alt={title}
          src={imgUrl}
        />
      </div>
    )
  }
}
