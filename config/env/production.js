'use strict';

const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanPlugin = require('clean-webpack-plugin');
const strip = require('strip-loader');
const projectRootDirectory = path.resolve();
const prefix = 'compomatic-widget-';
const OfflinePlugin = require('offline-plugin');


let universal = {
  __CLIENT__: true,
  __DEVTOOLS__: false,
  __SERVER__: false,
  API_URL: JSON.stringify('http://13.74.187.25:3010'),
  SOCKET_URL: JSON.stringify('http://nethack-development.northeurope.cloudapp.azure.com:3000/'),
  CLIENT_URL: JSON.stringify('http://nethack-development.northeurope.cloudapp.azure.com:3020/'),
  ADMIN_URL: JSON.stringify('http://nethack-development.northeurope.cloudapp.azure.com:3020/'),
  STRIPE_PUBLIC_KEY: JSON.stringify(''),
  FACEBOOK_APP_ID: JSON.stringify('1753852111608892')
};

if (process.env.API_ENV === 'development') {
  universal = {
    __CLIENT__: true,
    __DEVTOOLS__: true,
    __SERVER__: false,
    API_URL: JSON.stringify('http://13.74.187.25:3010'),
    CLIENT_URL: JSON.stringify('http://nethack-development.northeurope.cloudapp.azure.com:3020/')
  };
}


let jsLoader = {
  test: /\.js$/,
  loaders: [strip.loader('debug'), 'babel'],
  exclude: /^(?=.*?\bnode_modules\b)((?!nethack-frontend-core).)*$/,
  include: projectRootDirectory
};


if (process.env.NAME_PROJECT === 'widget') {
  jsLoader = {
    test: /\.js$/,
    loaders: [`react-prefix?prefix=${prefix}`, strip.loader('debug'), 'babel'],
    exclude: /^(?=.*?\bnode_modules\b)((?!(nethack-frontend-core|nethack-admin)).)*$/,
    include: projectRootDirectory
  }
}

module.exports = {
  secure: {
    ssl: false,
    privateKey: './config/sslcerts/key.pem',
    certificate: './config/sslcerts/cert.pem'
  },
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'combined',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      stream: 'access.log'
    }
  },
  files: {
    build: {
      js: 'build/*.js',
      css: 'build/*.css'
    }
  },
  universal: universal,
  webpack: {
    devtool: 'cheap-module-source-map',
    entry: {
      'main': [
        './public/application'
      ]
    },
    output: {
      path: path.join(projectRootDirectory, 'build'),
      filename: '[name].[hash].js',
      chunkFilename: '[name].[chunkhash].js',
      publicPath: '/build/'
    },
    module: {
      loaders: [jsLoader, {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!autoprefixer?browsers=last 2 version!sass?sourceMap=true&sourceMapContents=true')
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.json$/,
        loader: 'json'
      }, {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/octet-stream'
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      }, {
        test: /\.gif$/,
        loader: 'url-loader?mimetype=image/png'
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=image/svg+xml'
      }, {
        test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
        loader: 'imports?define=>false&this=>window'
      }],
      noParse: /\.min\.js/
    },
    progress: true,
    resolve: {
      modulesDirectories: [
        'public',
        'node_modules'
      ],
      extensions: ['', '.json', '.js']
    },
    plugins: [
      new webpack.DefinePlugin(universal),

      new webpack.DefinePlugin({
        'process.env': {
          'NODE_ENV': JSON.stringify('production')
        }
      }),

      new CleanPlugin('build', projectRootDirectory),
      new ExtractTextPlugin('[name].[chunkhash].css', {allChunks: true}),

      // ignore dev config
      new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),

      // optimizations
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.optimize.UglifyJsPlugin({
        beautify: false,
        comments: false,
        compress: {
            sequences: true,
            booleans: true,
            loops: true,
            unused: true,
            warnings: false,
            drop_console: true
        }
      }),

      new OfflinePlugin({
        publicPath: '/build/',
        relativePaths: false
      })
    ]
  }
};
