'use strict';

const fs = require('fs');
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const projectRootDirectory = path.resolve();
const prefix = 'compomatic-widget-';

const universal = {
  __CLIENT__: true,
  __DEVTOOLS__: true,
  __SERVER__: false,
  API_URL: JSON.stringify('http://localhost:3010'),
  SOCKET_URL: JSON.stringify('http://localhost:3000'),
  CLIENT_URL: JSON.stringify('http://localhost:3030'),
  ADMIN_URL: JSON.stringify('http://localhost:3020'),
  FACEBOOK_APP_ID: JSON.stringify('237317116672902')
};


let jsLoader = {
  test: /\.js$/,
  loaders: ['babel'],
  exclude: /^(?=.*?\bnode_modules\b)((?!nethack-frontend-core).)*$/,
  include: projectRootDirectory
};


if (process.env.NAME_PROJECT === 'widget') {
  jsLoader = {
    test: /\.js$/,
    loaders: [`react-prefix?prefix=${prefix}`, 'babel'],
    exclude: /^(?=.*?\bnode_modules\b)((?!(nethack-frontend-core|nethack-admin)).)*$/,
    include: projectRootDirectory
  }
}

let options = {
  log: {
    // Can specify one of 'combined', 'common', 'dev', 'short', 'tiny'
    format: 'dev',
    // Stream defaults to process.stdout
    // Uncomment to enable logging to a log on the file system
    options: {
      //stream: 'access.log'
    }
  },
  app: {
    title: 'Nethack - Local Environment'
  },
  universal: universal,
  webpack: {
    devtool: 'eval',
    entry: [
      'webpack-hot-middleware/client?reload=true',
      './public/application'
    ],
    output: {
      path: path.join(projectRootDirectory, 'build'),
      filename: 'main.js',
      publicPath: '/build/'
    },
    module: {
      loaders: [jsLoader, {test: /\.js$(\?v=\d+\.\d+\.\d+)?$/, exclude: /node_modules/, loaders: ['babel'] },{
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!autoprefixer?browsers=last 2 version!sass?sourceMap=true&sourceMapContents=true')
      }, {
        test: /\.css$/,
        loader: 'style-loader!css-loader'
      }, {
        test: /\.json$/,
        loader: 'json'
      }, {
        test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/font-woff'
      }, {
        test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=application/octet-stream'
      }, {
        test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'file'
      }, {
        test: /\.gif$/,
        loader: 'url-loader?mimetype=image/png'
      }, {
        test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url?limit=10000&minetype=image/svg+xml'
      }, {
        test: /masonry|imagesloaded|fizzy\-ui\-utils|desandro\-|outlayer|get\-size|doc\-ready|eventie|eventemitter/,
        loader: 'imports?define=>false&this=>window'
      }],
      noParse: /\.min\.js/
    },
    progress: true,
    resolve: {
      modulesDirectories: [
        'public',
        'node_modules'
      ],
      extensions: ['', '.json', '.js']
    },
    plugins: [
      new webpack.DefinePlugin(universal),
      new webpack.optimize.OccurenceOrderPlugin(),
      new webpack.HotModuleReplacementPlugin(),
      new ExtractTextPlugin('main.css')
    ]
  }
};

module.exports = options;
