'use strict';

module.exports = {
  app: {
    title: 'Nethack',
    description: 'Capperi on Napolilainen ruokaravintola Helsingin Oulukylässä, joka tarjoaa Suomen parhaat ja autenttiset Pizza Gourmet -pizzat!',
    keywords: '',
    googleAnalyticsTrackingID: process.env.GOOGLE_ANALYTICS_TRACKING_ID || 'GOOGLE_ANALYTICS_TRACKING_ID'
  },
  port: process.env.PORT,

  release: process.env.RELEASE || new Date().getDate() + '/' + (new Date().getMonth() + 1) + '/' + new Date().getFullYear(),

  files: {
    server: {
      routes: ['app/routes/**/!(core.route.js)', 'app/routes/**/core.route.js'],
      views: 'app/views'
    },
    build: {
      js: ['build/main.js'],
      css: ['build/main.css']
    }
  }
};
