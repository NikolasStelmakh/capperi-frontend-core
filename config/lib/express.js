'use strict';

/**
 * Module dependencies.
 */
const config = require('../config'),
  _ = require('lodash'),
  express = require('express'),
  morgan = require('morgan'),
  bodyParser = require('body-parser'),
  compress = require('compression'),
  methodOverride = require('method-override'),
  cookieParser = require('cookie-parser'),
  helmet = require('helmet'),
  path = require('path'),
  chalk = require('chalk'),
  webpack = require('webpack');


/**
 * Configure webpack
 */
module.exports.initWebpack = function (app, resolve, reject) {
  let child, bundleStart;

  for (var prop in config.universal) {
    global[prop] = config.universal[prop];
  }

  global.__CLIENT__ = false;
  global.__SERVER__ = true;
  global.API_URL = global.API_URL.split('"').join('');
  global.CLIENT_URL = global.CLIENT_URL.split('"').join('');
  global.ADMIN_URL = global.CLIENT_URL.split('"').join('');
  global.FACEBOOK_APP_ID = global.FACEBOOK_APP_ID.split('"').join('');

  if (process.env.NODE_ENV !== 'local') {
    resolve(true);
  } else {
    let compiler = webpack(config.webpack);

    compiler.plugin('compile', function () {
      console.log(chalk.green('Bundling ...'));
      bundleStart = Date.now();
    });

    app.use(require('webpack-dev-middleware')(compiler, {noInfo: true, publicPath: config.webpack.output.publicPath}));
    app.use(require('webpack-hot-middleware')(compiler));

    compiler.plugin('done', () => {
      console.log(chalk.green('Bundled in ' + (Date.now() - bundleStart) + 'ms!'));

      if (process.env.NODE_ENV === 'local') {
        Object.keys(require.cache).forEach(id => {
          if (/\/(public|compomatic-frontend-core)\//.test(id)) {
            delete require.cache[id];
          }
        });
      }
      resolve(true);
    });
  }
};

/**
 * Configure the static files
 */
module.exports.initStaticFiles = function () {
  if ((typeof config.files.build.js === 'function') && (typeof config.files.build.css === 'function')) {
    global.jsFile = config.files.build.js();
    global.cssFile = config.files.build.css();
  } else {
    global.jsFile = config.files.build.js;
    global.cssFile = config.files.build.css;
  }
};

/**
 * Initialize local variables
 */
module.exports.initLocalVariables = function (app) {

  // Passing the request url to environment locals
  app.use(function (req, res, next) {
    res.locals.host = req.protocol + '://' + req.hostname;
    res.locals.url = req.protocol + '://' + req.headers.host + req.originalUrl;
    next();
  });
};

/**
 * Initialize application middleware
 */
module.exports.initMiddleware = function (app) {
  // Showing stack errors
  app.set('showStackError', true);

  // Enable jsonp
  app.enable('jsonp callback');

  // Should be placed before express.static
  app.use(compress({
    filter: function (req, res) {
      return (/json|text|javascript|css|font|svg/).test(res.getHeader('Content-Type'));
    },
    level: 9
  }));

  // Environment dependent middleware
  if (process.env.NODE_ENV === 'production') {
    app.use(morgan('combined'));
    app.locals.cache = 'memory';
  } else {
    // Enable logger (morgan)
    app.use(morgan('dev'));

    // Disable views cache
    app.set('view cache', false);
  }

  // Request body parsing middleware should be above methodOverride
  app.use(bodyParser.urlencoded({
    extended: true
  }));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
};

/**
 * Configure the static folders
 */
module.exports.initStaticFolders = function (app) {
  const SIX_MONTHS = 15778476000;

  // Setting the static folder
  app.use('/build', express.static(path.resolve('./build'), {
    maxAge: SIX_MONTHS
  }));
  app.use('/images', express.static(path.resolve('./node_modules/nethack-frontend-core/images'), {
    maxAge: SIX_MONTHS
  }));
  app.use('/', express.static(path.resolve('./txt')));
};

/**
 * Configure Helmet headers configuration
 */
module.exports.initHelmetHeaders = function (app) {
  // Use helmet to secure Express headers
  const SIX_MONTHS = 15778476000;

  app.use(helmet.xframe());
  app.use(helmet.xssFilter());
  app.use(helmet.nosniff());
  app.use(helmet.ienoopen());
  app.use(helmet.hsts({
    maxAge: SIX_MONTHS,
    includeSubdomains: true,
    force: true
  }));
  app.disable('x-powered-by');
};

/**
 * Configure the modules server routes
 */
module.exports.initModulesServerRoutes = function (app) {
  // Globbing routing files
  config.files.server.routes.forEach(function (routePath) {
    require(path.resolve(routePath))(app);
  });
};

/**
 * Configure error handling
 */
module.exports.initErrorRoutes = function (app) {
  app.use(function (err, req, res, next) {
    // If the error object doesn't exists
    if (!err) return next();

    if (err instanceof Error) {
      // Log it
      console.error(err.stack);

      // Send error
      return res.status(500).json({
        error: err.message
      });
    }

    return res.status(400).json({error: err});
  });
};

/**
 * Configure client application
 */
module.exports.initClientApp = function (app) {
  new Promise((resolve, reject) => {
    return this.initWebpack(app, resolve, reject);
  })
    .then(() => {

      // Initialize static files
      this.initStaticFiles(app);

    }).catch((err) => {
      console.error(chalk.red('Webpack compile error: ' + err));
      process.exit(-1);
    });
};


/**
 * Initialize the Express application
 */
module.exports.init = function () {
  // Initialize express app
  var app = express();

  // Initialize client application
  this.initClientApp(app);

  // Initialize local variables
  this.initLocalVariables(app);

  // Initialize Express middleware
  this.initMiddleware(app);

  // Initialize static folders
  this.initStaticFolders(app);

  // Initialize Helmet security headers
  this.initHelmetHeaders(app);

  // Initialize modules server routes
  this.initModulesServerRoutes(app);

  // Initialize error routes
  this.initErrorRoutes(app);


  return app;
};
