'use strict';

/**
 * Module dependencies.
 */
/*var newrelic;

if (process.env.NODE_ENV === 'production') {
  newrelic = require('newrelic');
}*/

var config = require('../config'),
  express = require('./express'),
  chalk = require('chalk');


// Start app
module.exports.start = () => {
  let app = express.init();

  // Start the app by listening on <port>
  app.listen(config.port, () => {

    // Logging initialization
    console.log('--');
    console.log(chalk.green(config.app.title));
    console.log(chalk.green('Environment:\t\t\t' + process.env.NODE_ENV));
    console.log(chalk.green('Port:\t\t\t\t' + config.port));

    if (config.secure && config.secure.ssl === 'secure') {
      console.log(chalk.green('HTTPs:\t\t\t\ton'));
    }

    console.log('--');
  });

};