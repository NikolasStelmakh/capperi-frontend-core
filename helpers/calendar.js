'use strict';

import Moment from 'moment';

export function getDays(){
  return getDaysInMonth();
}

export function getDaysInMonth(month, year){
  let daysCount,
    days = [];

  if (month !== undefined && year !== undefined) {
    daysCount = Moment({month, year}).daysInMonth()
  } else {
    daysCount = Moment().daysInMonth()
  }

  for (let i = 1; i < daysCount + 1; i++) {
    days.push(i);
  }

  return days;
}

export function getToday() {
  return new Date().getDate();
}

export function getMonths() {
  return Moment.monthsShort();
}

export function getCurrentMonth() {
  return Moment().month() + 1;
}

export function getMonth(date) {
  return Moment.utc(date).format('M');
}

export function getYear(date) {
  return Moment.utc(date).format('YYYY');
}

export function getDay(date) {
  return Moment.utc(date).format('D');
}

export function getCurrentYear() {
  return new Date().getFullYear();
}

export function getDayFromToday(days) {
  return Moment().add(days, 'days').format('D');
}

export function getMonthFromToday(days) {
  return Moment().add(days, 'days').format('M');
}

export function getYearFromToday(days) {
  return Moment().add(days, 'days').format('YYYY');
}

export function getYears() {
  const maxYears = 3;

  let currentYear = getCurrentYear(),
    years = [];

  for (let i = 0; i < maxYears; i++) {
    years.push( currentYear + i );
  }

  return years;
}
